package com.webservice.methods;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author {Komala Nand Pandey} webservice 30-Oct-2017 3:07:49 PM
 */
public class RestMethodType {

	public static Response POST(String url, String StringJSON) {
		RequestSpecification requestSpecification = RestAssured.given().body(StringJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.post(url);
	
		return response;
	}

	public static Response GET(String url) {
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.get(url);

		return response;

	}

	public static Response PUT(String url, String StringJSON) {
		RequestSpecification requestSpecification = RestAssured.given().body(StringJSON);
		requestSpecification.contentType(ContentType.JSON);
		Response response = requestSpecification.put(url);

		return response;
	}
	
	public static Response GET(String url,String querykey, String value,String apiKey){
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification.contentType(ContentType.JSON);
		requestSpecification.param(querykey, value);
		requestSpecification.param(apiKey);
		requestSpecification.when();
		Response response = requestSpecification.get(url);
		
		return response;
	}

}
