/**
 * 
 */
package com.webservice.ServiceTestcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.google.gson.Gson;
import com.webservice.responsepojo.login.CustomerWrapper;
import com.webservice.responsepojo.login.Customers;
import com.webservice.responsepojo.login.LoginResponsePojo;
import com.webservice.services.Service;
import io.restassured.response.Response;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 27-Oct-2017 5:26:22 PM
 */
public class LoginAPITest {
	Service service;
	Response responseData;
	
	@Test
	public void loginAPITest(){
		service = new Service();
		responseData = service.login("kamal@aitrg.com", "App", "ait123");
//		System.out.println(responseData.asString());
		
		Gson gson = new Gson();
		LoginResponsePojo data = gson.fromJson(responseData.asString(),LoginResponsePojo.class);
		CustomerWrapper customerWrapper = data.getCustomerWrapper();
		Customers getCustomers = customerWrapper.getCustomers();
	
		System.out.println(getCustomers.getFirstName());
		System.out.println(getCustomers.getLastName());
		System.out.println(getCustomers.getGender());
		System.out.println(getCustomers.getLoginType());
		System.out.println(getCustomers.getRegistrationEmailSend());
		System.out.println(getCustomers.getCreateDate());
		
		Assert.assertEquals(getCustomers.getFirstName(), "kamal");
		Assert.assertEquals(getCustomers.getFirstName(), "kamal");
		Assert.assertEquals(getCustomers.getLastName(), "pandey");
		Assert.assertEquals(getCustomers.getGender(), "M");
		Assert.assertEquals(getCustomers.getLoginType(), "EMAIL");
		Assert.assertEquals(getCustomers.getRegistrationEmailSend(), "SENT");
		Assert.assertEquals(getCustomers.getCreateDate(), "2017-10-27T13:03:56+05:30");
				
	}
}
