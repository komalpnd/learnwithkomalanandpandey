/**
 * 
 */
package com.webservice.ServiceTestcases;
import com.google.gson.Gson;
import com.webservice.responsepojo.allProducts.AllProducts;
import com.webservice.responsepojo.allProducts.Banner;
import com.webservice.responsepojo.allProducts.Category;
import com.webservice.responsepojo.allProducts.Category_;
import com.webservice.responsepojo.allProducts.Category__;
import com.webservice.responsepojo.allProducts.Market;
import com.webservice.services.Service;
import io.restassured.response.Response;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 30-Oct-2017 10:19:55 AM
 */
public class GetAllProducts {
	Service service;
	Response responseData;
	
	@Test
	public void getAllProducts(){
		service = new Service();
		responseData = service.getAllProducts();		
		System.out.println(responseData.asString());			
		Gson gson = new Gson();
		AllProducts data = gson.fromJson(responseData.asString(), AllProducts.class);
		List<Category> category = data.getCategories();	
		
		for(int i=0;i<=category.size(); i++){
			if(i==0){
				Assert.assertEquals((category.get(i).getCategoryId()),"1");
				Assert.assertEquals((category.get(i).getCategoryName()),"Women");
				Assert.assertEquals((category.get(i).getCategoryType()),"CATEGORY");					
			}else if(i==1){
				Assert.assertEquals((category.get(i).getCategoryId()),"2");
				Assert.assertEquals((category.get(i).getCategoryName()),"Men");
				Assert.assertEquals((category.get(i).getCategoryType()),"CATEGORY");				
			}else if(i==2){
				Assert.assertEquals((category.get(i).getCategoryId()),"4");
				Assert.assertEquals((category.get(i).getCategoryName()),"Kids");
				Assert.assertEquals((category.get(i).getCategoryType()),"CATEGORY");			
			}else if(i==3){
				Assert.assertEquals((category.get(i).getCategoryId()),"10");
				Assert.assertEquals((category.get(i).getCategoryName()),"Brands");
				Assert.assertEquals((category.get(i).getCategoryType()),"CATEGORY");			
			}else if(i==4){
				Assert.assertEquals((category.get(i).getCategoryId()),"7");
				Assert.assertEquals((category.get(i).getCategoryName()),"Stores");
				Assert.assertEquals((category.get(i).getCategoryType()),"CATEGORY");			
			}else if(i==5){
				Assert.assertEquals((category.get(i).getCategoryId()),"11");
				Assert.assertEquals((category.get(i).getCategoryName()),"Bollywood");
				Assert.assertEquals((category.get(i).getCategoryType()),"EVENT");
			}
				
		}
		
		List<Category_> category_ = category.get(0).getCategories();
		for(int j=0;j<category_.size(); j++){	
			if(j==0){
				Assert.assertEquals((category_.get(j).getSubCateogryId()),"1");
				Assert.assertEquals((category_.get(j).getSubCatName()),"SHOP BY CATEGORY");
				Assert.assertEquals((category_.get(j).getType()),"CATEGORY");
			}else if(j==1){
				Assert.assertEquals((category_.get(j).getSubCateogryId()),"11");
				Assert.assertEquals((category_.get(j).getSubCatName()),"SALES");
				Assert.assertEquals((category_.get(j).getType()),"EVENT");
			}			
		}
		
		List<Category__> category__ = category_.get(0).getCategories();
		
		for(int k=0; k<category__.size(); k++){
			if(k==0){
				Assert.assertEquals((category__.get(k).getCategoryId()),"9");
				Assert.assertEquals((category__.get(k).getCategoryName()),"Ethnic Wear");
				Assert.assertEquals((category__.get(k).getMainCategory()),"Women");
				Assert.assertEquals((category__.get(k).getIsCategory().toString()),"1");
				Assert.assertEquals((category__.get(k).getSortPriority()),"1");
			}else if(k==1){
				Assert.assertEquals((category__.get(k).getCategoryId()),"10");
				Assert.assertEquals((category__.get(k).getCategoryName()),"Western Wear");
				Assert.assertEquals((category__.get(k).getMainCategory()),"Women");
				Assert.assertEquals((category__.get(k).getIsCategory().toString()),"1");
				Assert.assertEquals((category__.get(k).getSortPriority()),"2");
			}
		}	
		
		Market market = data.getMarket();
		List<Banner> banner = market.getBanner();		
		
		for(Banner item:banner){
			System.out.println(item.getBannerpos());
			System.out.println(item.getImageUrl());
			System.out.println(item.getIsBanner());
		}
		
	}	

}
