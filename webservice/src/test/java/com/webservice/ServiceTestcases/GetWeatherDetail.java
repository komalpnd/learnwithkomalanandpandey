/**
 * 
 */
package com.webservice.ServiceTestcases;

import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.webservice.responsepojo.weatherDetail.WeatherDetail;
import com.webservice.services.Service;

import io.restassured.response.Response;
import junit.framework.Assert;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 31-Oct-2017 12:46:36 PM
 */
public class GetWeatherDetail {
	Service service;
	Response weatherResponseData;
	
	
	/**
	 * This testcase will find weather report based on city name
	 * The Weather report is dynamic in nature therefore apply assertion based on current condition.
	 */
	@Test
	public void verifyWeatherByCityName(){
		service = new Service();		
		weatherResponseData = service.getWeatherByCityName();
		System.out.println(weatherResponseData.asString());
		Gson gson = new Gson();
		WeatherDetail gsonData = gson.fromJson(weatherResponseData.asString(), WeatherDetail.class);
		
		//Verifying RESTful response data using assertion.
		
		Assert.assertEquals((gsonData.getMain().getPressure().toString()),"1013");
		Assert.assertEquals(gsonData.getName(), "Connaught Place");	
		Assert.assertEquals(gsonData.getMain().getTemp(), 303.15);
		Assert.assertEquals(gsonData.getSys().getCountry(), "IN");
		Assert.assertEquals(gsonData.getWeather().get(0).getMain(), "Smoke");		
	}
}
