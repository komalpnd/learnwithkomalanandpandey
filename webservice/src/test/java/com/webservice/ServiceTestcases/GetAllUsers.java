/**
 * 
 */
package com.webservice.ServiceTestcases;

import org.testng.annotations.Test;

import com.webservice.services.Service;

import io.restassured.response.Response;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 30-Oct-2017 12:52:26 PM
 */
public class GetAllUsers {
	Service service;
	Response allUsersData;
	
	@Test
	public void getAllUsers(){
		service = new Service();		
		allUsersData = service.getAllUsersService();
		
		System.out.println(allUsersData.asString());
	}
	
}
