/**
 * 
 */
package com.webservice.responsepojo.weatherDetail;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 31-Oct-2017 1:14:27 PM
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Clouds {

	@SerializedName("all")
	@Expose
	private Integer all;

	public Integer getAll() {
		return all;
	}

	public void setAll(Integer all) {
		this.all = all;
	}
}
