/**
 * 
 */
package com.webservice.responsepojo.allProducts;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 30-Oct-2017 11:28:27 AM
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Market {

	@SerializedName("banner")
	@Expose
	private List<Banner> banner = null;

	public List<Banner> getBanner() {
		return banner;
	}

	public void setBanner(List<Banner> banner) {
		this.banner = banner;
	}
}