/**
 * 
 */
package com.webservice.services;
import org.json.JSONObject;
import com.webservice.methods.RestMethodType;
import com.webservice.requestpojo.login.Login;
import com.webservice.requestpojo.login.LoginPojo;
import io.restassured.response.Response;


/**
 * @author {Komala Nand Pandey} webservice 27-Oct-2017 3:29:01 PM
 */
public class Service {
	/**
	 * This API will perform login operation
	 * 
	 * @param loginId
	 * @param loginSource
	 * @param password
	 * @return
	 */

	public Response login(String loginId, String loginSource, String password) {
		try {
			LoginPojo loginPojo = new LoginPojo();
			Login login = new Login();

			login.setLoginId(loginId);
			login.setLoginSource(loginSource);
			login.setPassword(password);

			loginPojo.setLogin(login);
			JSONObject jsonObject = new JSONObject(loginPojo);
			Response response = RestMethodType.POST(Utils.baseURL + Utils.EndpointURLs.LOGIN.getResourcePath(),
					jsonObject.toString());
			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * This method will perform to verify correctness of all products.
	 * @return
	 */
	public Response getAllProducts() {
		try {
			Response response = RestMethodType
					.GET(Utils.baseURL + Utils.EndpointURLs.ALL_PRODUCTS.getResourcePath());
			
			return response;
		} catch (Exception e) {
			System.out.println("getall products failed");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This will retrieve all users.
	 * @return
	 */

	public Response getAllUsersService() {
		try {
			Response response = RestMethodType.GET(Utils.baseURL + Utils.EndpointURLs.ALL_USERS.getResourcePath());
			return response;
		} catch (Exception e) {
			System.out.println("get all services failed");
			e.printStackTrace();
		}
		return null;
	}
	
	public Response getWeatherByCityName(){
		String url =	Utils.weatherBaseURL+Utils.EndpointURLs.WEATHER_BY_CITY.getResourcePath()+
						Utils.QueryParams.WEATHER_QUERY.getQueryPath()+
						Utils.QueryParams.WEATHER_VALUE.getQueryPath()+
						Utils.apiKey();
		Response response = RestMethodType.GET(url);			
		
		
		return response;
	}
}





















