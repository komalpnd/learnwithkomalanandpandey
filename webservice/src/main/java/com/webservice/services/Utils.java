/**
 * 
 */
package com.webservice.services;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 27-Oct-2017 5:17:29 PM
 */
public class Utils {
	
	public static final String key = "appid";
	public static final String value = "fa459c381465a6251b2756f50e1b92e1";
	
	public static final String baseURL = "http://www.fashionandyou.com";
	public static final String weatherBaseURL = "http://api.openweathermap.org";
	
	public enum EndpointURLs{
		
		LOGIN("/api/login/business?operationType=login"),
		ALL_PRODUCTS("/search-new/web/menu?p-type=all"),
		ALL_USERS("/api/users"),
		WEATHER_BY_CITY("/data/2.5/weather");
		
		String resourcePath;
		
		EndpointURLs(String resourcePath) {
			this.resourcePath = resourcePath;
		}

		public String getResourcePath() {
			return resourcePath;
		}
		
		public String getResourcePath(String data) {
			return resourcePath+data;
		}		
	}
	
	public enum QueryParams{
		WEATHER_QUERY("?q="),
		WEATHER_VALUE("India&");
		
		String queryPath;
		
		private QueryParams(String queryPath) {
			this.queryPath = queryPath;
		}
		
		public String getQueryPath() {
			return queryPath;
		}

		public void setQueryPath(String queryPath) {
			this.queryPath = queryPath;
		}

		
		
		
	}
	
	public static String apiKey(){
		return key+"="+value;
	}
	
}
