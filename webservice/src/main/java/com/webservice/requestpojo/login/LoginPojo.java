/**
 * 
 */
package com.webservice.requestpojo.login;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 27-Oct-2017 3:34:52 PM
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginPojo {

	@SerializedName("login")
	@Expose
	private Login login;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

}