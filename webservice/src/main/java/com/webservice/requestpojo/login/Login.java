/**
 * 
 */
package com.webservice.requestpojo.login;

/**
 * @author {Komala Nand Pandey}
 * webservice
 * 27-Oct-2017 3:36:45 PM
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

	@SerializedName("loginId")
	@Expose
	private String loginId;
	
	@SerializedName("password")
	@Expose
	private String password;
	
	@SerializedName("loginSource")
	@Expose
	private String loginSource;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

}